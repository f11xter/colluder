/** @returns {asserts value} */
export function assert(value) {
  if (!value) {
    throw Error("Assertion failed");
  }
}
