export const Collections = {
  Clues: "clues",
  Crosswords: "crosswords",
  Messages: "messages",
  Squares: "squares",
};

export const DEFAULT_COLOUR = "#eeff00";

/** @typedef {string} RecordId */

/**
 * @typedef {Object} CluesRecord
 * @property {number} number
 * @property {string} content
 * @property {boolean} isAcross
 * @property {string} length A string representing the length of the words of the clue, eg. `"(3,6)"`
 * @property {RecordId} crossword
 */

/** @typedef {import("./pb_sdk/pocketbase.es").RecordModel & CluesRecord} CluesResponse */

/**
 * @typedef {Object} CrosswordsRecord
 * @property {string} [title]
 * @property {number} width
 * @property {number} height
 */

/** @typedef {import("./pb_sdk/pocketbase.es").RecordModel & CrosswordsRecord} CrosswordsResponse */

/**
 * @typedef {Object} MessagesRecord
 * @property {string} name
 * @property {string} content
 * @property {RecordId} crossword
 * @property {RecordId} [clue]
 */

/** @typedef {import("./pb_sdk/pocketbase.es").RecordModel & MessagesRecord} MessagesResponse */

/**
 * @typedef {Object} SquaresRecord
 * @property {number} row
 * @property {number} col
 * @property {boolean} isClueStart
 * @property {number} [across]
 * @property {number} [down]
 * @property {string} [value]
 * @property {string} [colour]
 * @property {RecordId} crossword
 */

/** @typedef {import("./pb_sdk/pocketbase.es").RecordModel & SquaresRecord} SquaresResponse */
