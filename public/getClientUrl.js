/** @returns {string} */
export function getClientUrl() {
  return window.location.hostname === "f11xter.gitlab.io" ||
    window.location.hostname === "colluder.felixwaller.dev"
    ? "https://colluder.pockethost.io"
    : "http://localhost:8090";
}
