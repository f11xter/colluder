import { assert } from "../assert.js";
import { create } from "./create.js";

/**
 * @typedef {Object} Clue
 * @property {number} number
 * @property {number} length
 * @property {string} [words]
 * @property {string} [content]
 */

/**
 * @param {{ across: Clue[]; down: Clue[] }} clues
 * @param {{ title: string; width: number; height: number; squares: import("./draw").Square[][] }} fields
 */
export function editClues(clues, fields) {
  const cluesEl = document.getElementById("clues");
  const formEl = document.querySelector("#clues form");
  const acrossEl = document.getElementById("across");
  const downEl = document.getElementById("down");

  assert(cluesEl && formEl instanceof HTMLFormElement && acrossEl && downEl);

  cluesEl.classList.add("active");

  acrossEl.insertAdjacentHTML(
    "beforeend",
    clues.across
      .map(
        ({ number, length }) =>
          `<span>${number}.</span>
					<input type=text name="${number}a-content" required spellcheck="true">
					<input type=text name="${number}a-length" value="${length}" required>`
      )
      .join("")
  );

  downEl.insertAdjacentHTML(
    "beforeend",
    clues.down
      .map(
        ({ number, length }) =>
          `<span>${number}.</span>
					<input type=text name="${number}d-content" required spellcheck="true">
					<input type=text name="${number}d-length" value="${length}" required>`
      )
      .join("")
  );

  cluesEl.addEventListener("submit", (e) => {
    e.preventDefault();

    const data = new FormData(formEl);

    data.forEach((value, key) => {
      const [clueId, clueType] = key.split("-");
      /** @type {Clue | undefined} */
      let clue = undefined;

      const findClue = ({ number }) =>
        clueId.replace(/[ad]/, "") === number.toString();

      if (clueId.includes("a")) {
        clue = clues.across.find(findClue);
      } else {
        clue = clues.down.find(findClue);
      }

      if (clue === undefined || typeof value !== "string") {
        return;
      }

      if (clueType === "content") {
        clue.content = value;
      } else if (clueType === "length") {
        clue.words = value;
      }
    });

    cluesEl.classList.remove("active");

    create(clues, fields);
  });
}
