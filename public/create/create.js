import { assert } from "../assert.js";
import { getClientUrl } from "../getClientUrl.js";
import Client from "https://esm.sh/pocketbase@0.21.2";
import { Collections } from "../types.js";

/**
 * @param {{ across: import("./clues.js").Clue[]; down: import("./clues.js").Clue[] }} clues
 * @param {{ title: string; width: number; height: number; squares: import("./draw").Square[][] }} fields
 */
export async function create(clues, fields) {
  const loadingEl = document.getElementById("loading");

  assert(loadingEl);

  loadingEl.classList.add("active");

  const data = {
    clues,
    title: fields.title,
    height: fields.height,
    width: fields.width,
    squares: fields.squares
      .flatMap((squares, row) => squares.map((s, col) => ({...s, row, col})))
      .filter(({isWhite}) => isWhite),
  };

  fetch(getClientUrl() + "/create", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(data),
  })
  .then((res) => res.json())
  .then((data) => window.location.href = `/puzzle?id=${data.id}`);
}

/**
 * @param {import("./clues.js").Clue} clue
 * @param {boolean} isAcross
 * @param {import("../types.js").RecordId} crosswordId
 * @param {Client} client
 * @returns {Promise<import("../pb_sdk/pocketbase.es.js").RecordModel>}
 */
async function createClue(clue, isAcross, crosswordId, client) {
  /** @type {import("../types.js").CluesRecord} */
  const clueData = {
    number: clue.number,
    content: clue.content ?? "",
    length: clue.words ?? clue.length.toString(),
    isAcross,
    crossword: crosswordId,
  };

  return client
    .collection(Collections.Clues)
    .create(clueData, { requestKey: `clue ${isAcross} ${clue.number}` });
}
