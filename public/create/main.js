import { assert } from "../assert.js";
import { drawCrossword } from "./draw.js";

const detailsEl = document.getElementById("details");
const formEl = document.getElementById("details-form");
const errorEl = document.getElementById("error");

assert(detailsEl && formEl instanceof HTMLFormElement && errorEl);

formEl.addEventListener("submit", (e) => {
  e.preventDefault();

  const data = new FormData(formEl);

  const title = data.get("title")?.valueOf() ?? "";
  const width = Number(data.get("width") || Number.NaN);
  const height = Number(data.get("height") || Number.NaN);

  const errors = [];

  if (Number.isNaN(width)) {
    errors.push("Width must be a number between 1 and 100");
  }

  if (Number.isNaN(height)) {
    errors.push("Height must be a number between 1 and 100");
  }

  if (errors.length > 0) {
    errorEl.innerHTML = errors.join("<br>");
    return;
  }

  detailsEl.classList.remove("active");

  if (typeof title === "string") {
    drawCrossword(title, width, height);
  } else {
    drawCrossword("", width, height);
  }
});
