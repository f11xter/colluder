import { assert } from "../assert.js";
import { editClues } from "./clues.js";

/**
 * @typedef {Object} Square
 * @property {boolean} isWhite
 * @property {boolean} isClueStart
 * @property {number} [across]
 * @property {number} [down]
 */

/**
 * @param {string} title
 * @param {number} width
 * @param {number} height
 */
export function drawCrossword(title, width, height) {
  const drawEl = document.getElementById("draw");
  const crosswordWrapperEl = document.getElementById("crossword-wrapper");
  const crosswordEl = document.getElementById("crossword");
  const nextEl = document.getElementById("next");

  assert(drawEl && crosswordWrapperEl && crosswordEl && nextEl);

  drawEl.classList.add("active");

  /** @type {(Square)[][]} */
  const squares = Array.from({ length: height }, () =>
    Array.from({ length: width }, () => ({
      isWhite: true,
      isClueStart: false,
    }))
  );

  /**
   * @type {{
   *  across: import("./clues.js").Clue[];
   *  down: import("./clues.js").Clue[];
   * }}
   */
  let clues = { across: [], down: [] };

  crosswordWrapperEl.style.setProperty("--cols", squares[0].length.toString());

  crosswordEl.innerHTML = squares
    .flatMap((row, y) =>
      row.map(
        (square, x) => `<button
					type="button"
					data-x="${x}"
					data-y="${y}"
					data-white="${square.isWhite}"
					data-clue-start="${square.isClueStart}">
				</button>`
      )
    )
    .join("");

  clues = updateNumbering(squares);

  crosswordEl.addEventListener("click", (e) => {
    if (e.target === null || !(e.target instanceof HTMLButtonElement)) {
      return;
    }

    const x = Number(e.target.getAttribute("data-x") || Number.NaN);
    const y = Number(e.target.getAttribute("data-y") || Number.NaN);

    assert(!Number.isNaN(x) && !Number.isNaN(y));

    squares[y][x].isWhite = !squares[y][x].isWhite;
    e.target.setAttribute("data-white", squares[y][x].isWhite.toString());
    clues = updateNumbering(squares);
  });

  nextEl.addEventListener("click", () => {
    drawEl.classList.remove("active");
    editClues(clues, { title, width, height, squares });
  });
}

/**
 * @param {Square[][]} squares
 * @returns {{
 *  across: import("./clues.js").Clue[];
 *  down: import("./clues.js").Clue[];
 * }}
 */
function updateNumbering(squares) {
  /**
   * @type {{
   *  across: import("./clues.js").Clue[];
   *  down: import("./clues.js").Clue[];
   * }}
   */
  const clues = { across: [], down: [] };

  let clueNumber = 1;

  for (let row = 0; row < squares.length; row++) {
    for (let column = 0; column < squares[row].length; column++) {
      const squareEl = document.querySelector(
        `[data-x="${column}"][data-y="${row}"]`
      );

      assert(squareEl instanceof HTMLButtonElement);

      squareEl.innerText = "";

      // reset black squares

      if (!squares[row][column].isWhite) {
        squares[row][column] = {
          isWhite: false,
          isClueStart: false,
          across: undefined,
          down: undefined,
        };
        continue;
      }

      // propagate clue numbers through words

      if (
        squares[row - 1]?.[column] !== undefined &&
        squares[row - 1]?.[column].isWhite
      ) {
        squares[row][column].down = squares[row - 1][column].down;
        const clue = clues.down.find(
          ({ number }) => number === squares[row][column].down
        );
        if (clue) {
          clue.length++;
        }
      } else {
        squares[row][column].down = undefined;
      }

      if (
        squares[row][column - 1] !== undefined &&
        squares[row][column - 1].isWhite
      ) {
        squares[row][column].across = squares[row][column - 1].across;
        const clue = clues.across.find(
          ({ number }) => number === squares[row][column].across
        );
        if (clue) {
          clue.length++;
        }
      } else {
        squares[row][column].across = undefined;
      }

      // label starts of words

      let isClueStart = false;

      if (
        (squares[row - 1]?.[column] === undefined ||
          !squares[row - 1][column].isWhite) &&
        squares[row + 1]?.[column] !== undefined &&
        squares[row + 1][column].isWhite
      ) {
        isClueStart = true;
        squares[row][column].down = clueNumber;
        clues.down.push({ number: clueNumber, length: 1 });
      }

      if (
        (squares[row][column - 1] === undefined ||
          !squares[row][column - 1].isWhite) &&
        squares[row][column + 1] !== undefined &&
        squares[row][column + 1].isWhite
      ) {
        isClueStart = true;
        squares[row][column].across = clueNumber;
        clues.across.push({ number: clueNumber, length: 1 });
      }

      if (isClueStart) {
        squares[row][column].isClueStart = true;
        squareEl.innerText = clueNumber.toString();
        clueNumber++;
      } else {
        squares[row][column].isClueStart = false;
      }
    }
  }

  return clues;
}
