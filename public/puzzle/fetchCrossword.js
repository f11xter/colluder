import { getClientUrl } from "../getClientUrl.js";
import Client from "https://esm.sh/pocketbase@0.21.2";
import { Collections } from "../types.js";

/**
 * @param {import("../types").RecordId} id
 * @returns {Promise<{
 * 	details: import("../types").CrosswordsResponse;
 * 	clues: import("../types").CluesResponse[]
 * 	squares: import("../types").SquaresResponse[]
 * }>}
 */
export async function fetchCrossword(id) {
  const client = new Client(getClientUrl());

  /**
   * @type {[
   *   Promise<import("../types").CrosswordsResponse>,
   *   Promise<import("../types").CluesResponse[]>,
   *   Promise<import("../types").SquaresResponse[]>
   * ]}
   */
  const promises = [
    client.collection(Collections.Crosswords).getOne(id),

    client
      .collection(Collections.Clues)
      .getFullList({ filter: `crossword="${id}"` }),

    client
      .collection(Collections.Squares)
      .getFullList({ filter: `crossword="${id}"` }),
  ];

  const [details, clues, squares] = await Promise.all(promises);

  return { details, clues, squares };
}
