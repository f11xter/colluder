import { assert } from "../assert.js";
import { DEFAULT_COLOUR } from "../types.js";
import { chat } from "./chat.js";
import { fetchCrossword } from "./fetchCrossword.js";
import { showCrossword } from "./showCrossword.js";
import { sync, update } from "./sync.js";

const params = new URLSearchParams(window.location.search);

const puzzleId = params.get("id");

if (!puzzleId) {
  window.location.replace("/");
  throw Error("Puzzle id not found");
}

const crosswordEl = document.getElementById("crossword");

assert(crosswordEl instanceof HTMLDivElement);

const { details, clues, squares } = await fetchCrossword(puzzleId).catch(
  (e) => {
    window.location.replace("/");
    throw Error(e);
  }
);

clues.sort((a, b) =>
  a.isAcross && !b.isAcross
    ? -1
    : !a.isAcross && b.isAcross
    ? 1
    : a.number - b.number
);

showCrossword(details, clues, squares);
crossOffClues();

sync(squares);
chat(puzzleId, clues);

/** @type {number | undefined} */
let selectedClue = undefined;
/** @type {boolean | undefined} */
let isSelectedAcross = undefined;

selectClue(1, clues.find((x) => x.number === 1)?.isAcross ?? true, true);

let colour;
setColour(localStorage.getItem("colour") ?? DEFAULT_COLOUR);

window.addEventListener("focus", async () => {
  const { squares: newSquares } = await fetchCrossword(puzzleId).catch((e) => {
    window.location.replace("/");
    throw Error(e);
  });

  for (const newSquare of newSquares) {
    const el = document.querySelector(`[data-id="${newSquare.id}"] input`);
    const currentSquare = squares.find((x) => x.id === newSquare.id);

    if (
      !(el instanceof HTMLInputElement) ||
      !currentSquare ||
      (currentSquare.value === newSquare.value &&
        currentSquare.colour === newSquare.value)
    ) {
      continue;
    }

    currentSquare.value = newSquare.value;
    currentSquare.colour = newSquare.colour;

    el.value = currentSquare.value ?? "";
    el.parentElement?.style.setProperty(
      "--c",
      !currentSquare.colour || currentSquare.colour === DEFAULT_COLOUR
        ? "transparent"
        : currentSquare.colour + "40"
    );
  }
});

crosswordEl.querySelectorAll("div:not(.black)").forEach((el) => {
  el.addEventListener("click", () => {
    const across = Number(el.getAttribute("data-across") ?? Number.NaN);
    const down = Number(el.getAttribute("data-down") ?? Number.NaN);

    if (
      (isSelectedAcross !== true || selectedClue !== across) &&
      !Number.isNaN(across)
    ) {
      selectClue(across, true);
    } else if (!Number.isNaN(down)) {
      selectClue(down, false);
    }
  });
});

crosswordEl.addEventListener("keydown", (e) => {
  if (e.key === "ArrowRight" && e.ctrlKey) {
    selectNextClue();
  } else if (e.key === "ArrowLeft" && e.ctrlKey) {
    selectPrevClue();
  } else if (
    ["ArrowRight", "ArrowLeft", "ArrowUp", "ArrowDown"].includes(e.key) &&
    e.target &&
    e.target instanceof HTMLInputElement
  ) {
    const container = e.target.parentElement;
    const row = Number(container?.getAttribute("data-row") ?? Number.NaN);
    const col = Number(container?.getAttribute("data-col") ?? Number.NaN);

    assert(!Number.isNaN(row) && !Number.isNaN(col));

    /** @type {import("../types.js").SquaresResponse | undefined} */
    let square = undefined;
    /** @type {HTMLInputElement | null} */
    let squareEl = null;

    let [r, c] = [row, col];

    while (true) {
      [r, c] =
        e.key === "ArrowRight"
          ? [r, c + 1]
          : e.key === "ArrowLeft"
          ? [r, c - 1]
          : e.key === "ArrowDown"
          ? [r + 1, c]
          : [r - 1, c];

      if (r > details.height - 1) {
        r = 0;
        c = c < details.width - 1 ? c + 1 : 0;
      } else if (r < 0) {
        r = details.height - 1;
        c = c > 0 ? c - 1 : details.width - 1;
      } else if (c > details.width - 1) {
        c = 0;
        r = r < details.height - 1 ? r + 1 : 0;
      } else if (c < 0) {
        c = details.width - 1;
        r = r > 0 ? r - 1 : details.height - 1;
      }

      if (r === row && c === col) {
        throw Error("Failed to find square");
      }

      square = squares.find((x) => x.row === r && x.col === c);
      squareEl = document.querySelector(`[data-id="${square?.id}"] input`);

      if (square && squareEl) {
        break;
      }
    }

    if (square && squareEl) {
      if (isSelectedAcross && selectedClue !== square.across) {
        selectClue(square.across || square.down || 1, !!square.across);
      } else if (isSelectedAcross === false && selectedClue != square.down) {
        selectClue(square.down || square.across || 1, !square.down);
      }
      squareEl.focus();
    }
  }
});

crosswordEl.addEventListener("beforeinput", (e) => {
  if (!(e instanceof InputEvent && e.target instanceof HTMLInputElement)) {
    return;
  }

  e.preventDefault();

  const container = e.target.parentElement;

  const row = container?.getAttribute("data-row");
  const col = container?.getAttribute("data-col");
  const id = container?.getAttribute("data-id");

  assert(row && col && id);

  const current = squares.find((x) => x.id === id);

  assert(current);

  const prev = squares.find((x) =>
    isSelectedAcross
      ? x.row === Number(row) && x.col === Number(col) - 1
      : x.row === Number(row) - 1 && x.col === Number(col)
  );
  const prevEl = document.querySelector(`[data-id="${prev?.id}"] input`);

  const next = squares.find((x) =>
    isSelectedAcross
      ? x.row === Number(row) && x.col === Number(col) + 1
      : x.row === Number(row) + 1 && x.col === Number(col)
  );
  const nextEl = document.querySelector(`[data-id="${next?.id}"] input`);

  if (
    e.inputType === "deleteContent" ||
    e.inputType === "deleteContentBackward"
  ) {
    if (e.target.value !== "") {
      current.value = "";
      e.target.value = "";
      update(id, "", "");
    } else if (prev && prevEl && prevEl instanceof HTMLInputElement) {
      prev.value = "";
      prevEl.value = "";
      prevEl.focus();
      update(prev.id, "", "");
    }
  } else if (e.inputType === "deleteContentForward") {
    if (e.target.value !== "") {
      current.value = "";
      e.target.value = "";
      update(id, "", "");
    } else if (next && nextEl && nextEl instanceof HTMLInputElement) {
      next.value = "";
      nextEl.value = "";
      nextEl.focus();
      update(next.id, "", "");
    }
  } else {
    const l = e.data?.toLowerCase()[0];

    if (l !== undefined && l >= "a" && l <= "z") {
      if (e.target.value !== l) {
        current.value = l;
        e.target.value = l;
        update(id, l, colour);
      }

      if (nextEl && nextEl instanceof HTMLInputElement) {
        nextEl.focus();
      }
    }
  }

  crossOffClues();
});

document.querySelectorAll("#clues p").forEach((el) => {
  el.addEventListener("click", () => {
    const across = Number(el.getAttribute("data-across") ?? Number.NaN);
    const down = Number(el.getAttribute("data-down") ?? Number.NaN);

    if (!Number.isNaN(across)) {
      selectClue(across, true, true);
    } else if (!Number.isNaN(down)) {
      selectClue(down, false, true);
    }
  });
});

document.getElementById("crossword-size")?.addEventListener("input", (e) => {
  if (e.target instanceof HTMLInputElement) {
    document.body.style.setProperty(
      "--max-crossword-size-multiplier",
      e.target.value
    );
  }
});

document.getElementById("colour")?.addEventListener("change", (e) => {
  if (e.target instanceof HTMLInputElement) {
    setColour(e.target.value);
  }
});

document.getElementById("colour-reset")?.addEventListener("click", () => {
  setColour(DEFAULT_COLOUR);
});

document.querySelectorAll(".current-clue button.prev").forEach((el) => {
  el.addEventListener("click", () => {
    selectPrevClue();
  });
});

document.querySelectorAll(".current-clue button.next").forEach((el) => {
  el.addEventListener("click", () => {
    selectNextClue();
  });
});

export function crossOffClues() {
  for (const clue of clues) {
    const clueEl = document.querySelector(
      `#clues [data-${clue.isAcross ? "across" : "down"}="${clue.number}"]`
    );

    assert(clueEl);

    if (
      squares.filter(
        (x) =>
          clue.number === (clue.isAcross ? x.across : x.down) && x.value === ""
      ).length === 0
    ) {
      clueEl.classList.add("complete");
    } else {
      clueEl.classList.remove("complete");
    }
  }
}

/**
 * @param {number} clue
 * @param {boolean} isAcross
 * @param {boolean} [focus]
 */
export function selectClue(clue, isAcross, focus = false) {
  document
    .querySelectorAll(
      `[data-${isSelectedAcross ? "across" : "down"}="${selectedClue}"]`
    )
    .forEach((el) => {
      el.classList.remove("selected", "across", "across-start", "across-end", "down", "down-start", "down-end");
    });

  selectedClue = clue;
  isSelectedAcross = isAcross;

  Array.from(document
    .querySelectorAll(
      `[data-${isSelectedAcross ? "across" : "down"}="${selectedClue}"]`
    ))
    .forEach((el, i, arr) => {
      const direction = isSelectedAcross ? "across" : "down";
      el.classList.add("selected", direction);

      if (i === 0) {
        el.classList.add(direction + "-start");
      }
      // final array element is clue
      // so penultimate array element is last square
      else if (i === arr.length - 2) {
        el.classList.add(direction + "-end");
      }
    });

  document.querySelectorAll(".current-clue span").forEach((el) => {
    if (el instanceof HTMLSpanElement) {
      el.innerHTML =
        document.querySelector(
          `#clues [data-${isAcross ? "across" : "down"}="${clue}"]`
        )?.innerHTML ?? "";
    }
  });

  const chatClueEl = document.getElementById("clue-input");
  assert(chatClueEl instanceof HTMLSelectElement);

  chatClueEl.value =
    clues.find(
      ({ number, isAcross: across }) => number === clue && across === isAcross
    )?.id ?? "";

  if (focus) {
    const input = document.querySelector(
      `[data-${isAcross ? "across" : "down"}="${clue}"] input`
    );
    if (input && input instanceof HTMLInputElement) {
      input?.focus();
    }
  }
}

function selectNextClue() {
  const index = clues.findIndex(
    (x) => x.isAcross === isSelectedAcross && x.number === selectedClue
  );
  const next = clues.at(index < clues.length - 1 ? index + 1 : 0);
  if (next) {
    selectClue(next.number, next.isAcross, true);
  }
}

function selectPrevClue() {
  const index = clues.findIndex(
    (x) => x.isAcross === isSelectedAcross && x.number === selectedClue
  );
  const prev = clues.at(index - 1);
  if (prev) {
    selectClue(prev.number, prev.isAcross, true);
  }
}

/** @param {string} c */
function setColour(c) {
  colour = c;

  const colourEl = document.getElementById("colour");
  if (colourEl && colourEl instanceof HTMLInputElement) {
    colourEl.value = colour;
  }

  document.body.style.setProperty("--c-0", colour + "a0");
  document.body.style.setProperty("--c-1", colour + "80");
  document.body.style.setProperty("--c-2", colour);

  localStorage.setItem("colour", colour);
}
