import { getClientUrl } from "../getClientUrl.js";
import Client from "https://esm.sh/pocketbase@0.21.2";
import { Collections, DEFAULT_COLOUR } from "../types.js";
import { crossOffClues } from "./main.js";

const clientUrl = getClientUrl();
const client = new Client(clientUrl);

/** @param {import("../types.js").SquaresResponse[]} squares  */
export function sync(squares) {
  client.collection(Collections.Squares).subscribe(
    "*",
    /** @param {import("../pb_sdk/pocketbase.es.js").RecordSubscription<import("../types.js").SquaresResponse>} data  */
    (data) => {
      const el = document.querySelector(`[data-id="${data.record.id}"] input`);
      const square = squares.find((x) => x.id === data.record.id);

      if (el && el instanceof HTMLInputElement && square) {
        el.value = data.record.value ?? "";
        el.parentElement?.style.setProperty(
          "--c",
          !data.record.colour || data.record.colour === DEFAULT_COLOUR
            ? "transparent"
            : data.record.colour + "40"
        );

        square.value = data.record.value ?? "";
        crossOffClues();
      }
    }
  );
}

/**
 * @param {import("../types.js").RecordId} id
 * @param {string} value
 * @param {string} colour
 */
export async function update(id, value, colour) {
  try {
    await client
      .collection(Collections.Squares)
      .update(id, { value, colour: colour });
  } catch {
    console.error("Failed to sync with server");
  }
}
