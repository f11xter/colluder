import { assert } from "../assert.js";
import { getClientUrl } from "../getClientUrl.js";
import Client from "https://esm.sh/pocketbase@0.21.2";
import { Collections } from "../types.js";
import { selectClue } from "./main.js";

const clientUrl = getClientUrl();

/**
 * @param {import("../types").RecordId} puzzleId
 * @param {import("../types.js").CluesResponse[]} clues
 */
export async function chat(puzzleId, clues) {
  const client = new Client(clientUrl);

  const chatAnchor = document.getElementById("chat-anchor");
  const form = document.getElementById("chat-form");
  const input = document.getElementById("chat-input");

  assert(
    chatAnchor &&
      form instanceof HTMLFormElement &&
      input instanceof HTMLInputElement
  );

  form.addEventListener("submit", (e) => {
    e.preventDefault();

    let name = "anon";

    const nameEl = document.getElementById("name-input");
    if (nameEl && nameEl instanceof HTMLInputElement) {
      name = nameEl.value || "anon";
    }

    const formData = new FormData(form);
    const clue = formData.get("clue")?.toString();
    const content = formData.get("content")?.toString();

    assert(content);

    /** @type {import("../types.js").MessagesRecord} */
    const messageData = {
      name,
      content,
      clue,
      crossword: puzzleId,
    };

    client.collection(Collections.Messages).create(messageData);

    input.value = "";
  });

  /** @type {import("../types.js").MessagesResponse[]} */
  const messages = await client
    .collection(Collections.Messages)
    .getFullList({ filter: `crossword="${puzzleId}"` });

  chatAnchor.insertAdjacentHTML(
    "beforebegin",
    messages
      .map((msg) => {
        const clue = clues.find((x) => x.id === msg.clue);
        return `
          <p class="subgrid" data-clue="${msg.clue ?? ""}">
            <span>${msg.name}:</span>
            ${
              clue
                ? `<button type="button"
                    data-number="${clue.number}"
                    data-across="${clue.isAcross}"
                  >${clue.number}${clue.isAcross ? "a" : "d"}</button>`
                : "<place-holder></place-holder>"
            }
            <span>${msg.content}</span>
          </p>`;
      })
      .join("")
  );

  document.querySelectorAll("button[data-number]").forEach((el) =>
    el.addEventListener("click", () => {
      const number = el.getAttribute("data-number");
      const isAcross = el.getAttribute("data-across");

      assert(number !== null && isAcross !== null);

      selectClue(Number(number), isAcross === "true", true);
    })
  );

  // chat anchoring activates only once anchor is visible
  chatAnchor.scrollIntoView({ behavior: "instant" });
  window.scroll({ top: 0, behavior: "instant" });

  client.collection(Collections.Messages).subscribe(
    "*",
    /** @param {import("../pb_sdk/pocketbase.es.js").RecordSubscription<import("../types.js").MessagesResponse>} data  */
    (data) => {
      if (data.record.crossword === puzzleId && data.action === "create") {
        const clue = clues.find((x) => x.id === data.record.clue);

        chatAnchor.insertAdjacentHTML(
          "beforebegin",
          `<p class="subgrid" data-clue="${data.record.clue ?? ""}">
            <span>${data.record.name}:</span>
            ${
              clue
                ? `<button type="button"
                    id="${data.record.id}"
                    data-number="${clue.number}"
                    data-across="${clue.isAcross}"
                  >${clue.number}${clue.isAcross ? "a" : "d"}</button>`
                : "<place-holder></place-holder>"
            }
            <span>${data.record.content}</span>
            </p>`
        );

        if (!clue) {
          return;
        }

        document
          .getElementById(data.record.id)
          ?.addEventListener("click", () =>
            selectClue(clue.number, clue.isAcross, true)
          );
      }
    }
  );
}
