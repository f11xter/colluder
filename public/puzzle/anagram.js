import { assert } from "../assert.js";

/** @type {Array<{ value: string; struck: boolean }>} */
let letters = [];
/** @type {Array<{ value: string; invalid: boolean }>} */
let word = [];

const circleEl = document.getElementById("anagram-circle");
const wordEl = document.getElementById("anagram-word");

assert(circleEl && wordEl);

document.getElementById("anagram-input")?.addEventListener("input", (e) => {
  if (e.target instanceof HTMLInputElement) {
    letters = e.target.value
      .split("")
      .map((x) => ({ value: x, struck: false }));

    const diff = letters.length - word.length;

    if (diff > 0) {
      word.push(
        ...Array.from({ length: diff }, () => ({ value: "", invalid: false }))
      );
    } else if (diff < 0) {
      word.splice(letters.length, -diff);
    }

    draw();
    setStruck();
  }
});

document.getElementById("shuffle")?.addEventListener("click", () => {
  letters.sort(() => Math.random() - 0.5);
  drawCircle();
  setStruck();
});

document.getElementById("anagram-size")?.addEventListener("input", (e) => {
  if (e.target instanceof HTMLInputElement) {
    circleEl.style.setProperty("--size", e.target.valueAsNumber.toString());
  }
});

wordEl.addEventListener("keydown", (e) => {
  if (!(e.target instanceof HTMLInputElement)) {
    return;
  }

  let toFocus = undefined;

  if (e.key === "ArrowRight") {
    toFocus = e.target.nextElementSibling;
  } else if (e.key === "ArrowLeft") {
    toFocus = e.target.previousElementSibling;
  }

  if (toFocus instanceof HTMLInputElement) {
    toFocus.focus();
  }
});

const drawCircle = () => {
  circleEl.innerHTML = letters
    .map(
      (x, index, arr) =>
        `<p style="--rotation: ${index / arr.length}turn">
          <span data-index="${index}">${x.value}</span>
        </p>`
    )
    .join("");
};

const draw = () => {
  drawCircle();

  wordEl.innerHTML = word
    .map(
      (x, index) =>
        `<input
          type="text"
          maxlength="1"
          value="${x.value}"
          data-index="${index}"
        />`
    )
    .join("");

  wordEl.querySelectorAll("input").forEach((el) =>
    el.addEventListener("beforeinput", (e) => {
      if (!(e.target instanceof HTMLInputElement)) {
        return;
      }

      e.preventDefault();

      const prev = e.target.previousElementSibling;
      const next = e.target.nextElementSibling;

      if (
        e.inputType === "deleteContent" ||
        e.inputType === "deleteContentBackward"
      ) {
        if (e.target.value !== "") {
          e.target.value = "";
        } else if (prev instanceof HTMLInputElement) {
          prev.value = "";
          prev.focus();
        }
      } else if (e.inputType === "deleteContentForward") {
        if (e.target.value !== "") {
          e.target.value = "";
        } else if (next instanceof HTMLInputElement) {
          next.value = "";
          next.focus();
        }
      } else {
        const l = e.data?.toLowerCase()[0];

        if (l && l >= "a" && l <= "z") {
          if (e.target.value !== l) {
            e.target.value = l;
          }

          if (next instanceof HTMLInputElement) {
            next.focus();
          }
        }
      }

      const index = Number(e.target.getAttribute("data-index") ?? Number.NaN);

      if (!Number.isNaN(index)) {
        word[index] = { value: e.target.value, invalid: false };
        setStruck();
      }
    })
  );
};

const setStruck = () => {
  letters.forEach((x, i) => {
    x.struck = false;
    circleEl.querySelector(`[data-index="${i}"]`)?.classList.remove("struck");
  });

  word.forEach((l, i) => {
    const letterIndex = letters.findIndex(
      (x) => x.value === l.value && !x.struck
    );

    if (!l.value) {
      wordEl.querySelector(`[data-index="${i}"]`)?.classList.remove("invalid");
    } else if (letterIndex === -1) {
      l.invalid = true;
      wordEl.querySelector(`[data-index="${i}"]`)?.classList.add("invalid");
    } else {
      letters[letterIndex].struck = true;
      circleEl
        .querySelector(`[data-index="${letterIndex}"]`)
        ?.classList.add("struck");
    }
  });
};
