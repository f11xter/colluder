import { assert } from "../assert.js";
import { DEFAULT_COLOUR } from "../types.js";

/**
 * @param {import("../types").CrosswordsResponse} details
 * @param {import("../types").CluesResponse[]} clues
 * @param {import("../types").SquaresResponse[]} squares
 */
export function showCrossword(details, clues, squares) {
  const crosswordWrapperEl = document.getElementById("crossword-wrapper");
  const crosswordEl = document.getElementById("crossword");
  const acrossEl = document.getElementById("across");
  const downEl = document.getElementById("down");
  const selectEl = document.getElementById("clue-input");

  assert(
    crosswordWrapperEl &&
      crosswordEl &&
      acrossEl &&
      downEl &&
      selectEl
  );

  if (details.title) {
    document.title = `${details.title} - Colluder`;

    const h1El = document.querySelector("h1");

    if (h1El !== null) {
      h1El.textContent = details.title;
    }
  }

  crosswordWrapperEl.style.setProperty("--cols", details.width.toString());

  crosswordEl.innerHTML = Array.from({ length: details.height }, (_, row) =>
    Array.from({ length: details.width }, (_, col) => {
      const square = squares.find((s) => s.row === row && s.col === col);
      return square
        ? `<div
					data-row="${row}"
					data-col="${col}"
					${square.across ? `data-across="${square.across}"` : ""}
					${square.down ? `data-down="${square.down}"` : ""}
					${
            square.isClueStart
              ? `data-clue="${Math.max(square.across ?? 0, square.down ?? 0)}"`
              : ""
          }
					data-id="${square.id}"
          style="--c: ${
            !square.colour || square.colour === DEFAULT_COLOUR
              ? "transparent"
              : `${square.colour}40` ?? ""
          }"
				>
					<input type="text" maxlength="1" value="${square.value}" >
				</div>`
        : `<div class="black"></div>`;
    })
  )
    .flat()
    .join("");

  acrossEl.innerHTML = clues
    .filter(({ isAcross }) => isAcross)
    .sort((a, b) => a.number - b.number)
    .map(
      ({ number, content, length }) =>
        `<p data-across="${number}" class="subgrid"><span>${number}.</span><span>${content} (${length})</span></p>`
    )
    .join("");

  downEl.innerHTML = clues
    .filter(({ isAcross }) => !isAcross)
    .sort((a, b) => a.number - b.number)
    .map(
      ({ number, content, length }) =>
        `<p data-down="${number}" class="subgrid"><span>${number}.</span><span>${content} (${length})</span></p>`
    )
    .join("");

  const options = [
    `<option value="">---</option><hr>`,
    clues
      .filter(({ isAcross }) => isAcross)
      .sort((a, b) => a.number - b.number)
      .map(
        ({ number, id }) => `<option value="${id}">${number} Across</option>`
      ),
    "<hr>",
    clues
      .filter(({ isAcross }) => !isAcross)
      .sort((a, b) => a.number - b.number)
      .map(({ number, id }) => `<option value="${id}">${number} Down</option>`),
  ]
    .flat()
    .join("");

  selectEl.innerHTML = options;
}
