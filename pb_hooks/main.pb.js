/// <reference path="../pb_data/types.d.ts" />

routerAdd("POST", "/create", (c) => {
  try {
    const data = $apis.requestInfo(c).data;

    const crosswordsCollection = $app.dao().findCollectionByNameOrId("crosswords");
    const cluesCollection = $app.dao().findCollectionByNameOrId("clues");
    const squaresCollection = $app.dao().findCollectionByNameOrId("squares");

    const crossword = new Record(crosswordsCollection);
    const crosswordForm = new RecordUpsertForm($app, crossword);

    crosswordForm.loadData({
      title: data.title,
      width: data.width,
      height: data.height,
    });
    crosswordForm.submit();


    for (const clue of data.clues.across) {
      const clueForm = new RecordUpsertForm($app, new Record(cluesCollection));
      clueForm.loadData({
        number: clue.number,
        content: clue.content,
        length: clue.words || clue.length,
        isAcross: true,
        crossword: crossword.id,
      });
      clueForm.submit();
    }

    for (const clue of data.clues.down) {
      const clueForm = new RecordUpsertForm($app, new Record(cluesCollection));
      clueForm.loadData({
        number: clue.number,
        content: clue.content,
        length: clue.words || clue.length,
        isAcross: false,
        crossword: crossword.id,
      });
      clueForm.submit();
    }

    for (const square of data.squares) {
      const squareForm = new RecordUpsertForm($app, new Record(squaresCollection));
      squareForm.loadData({
        ...square,
        crossword: crossword.id,
      });
      squareForm.submit();
    }

    return c.json(200, { id: crossword.id });
  } catch {
    return c.noContent(500);
  }
})
