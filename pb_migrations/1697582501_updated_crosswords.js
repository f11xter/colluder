/// <reference path="../pb_data/types.d.ts" />
migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("3zwyqsaoko4dxc5")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "jycjljwj",
    "name": "width",
    "type": "number",
    "required": true,
    "presentable": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null,
      "noDecimal": true
    }
  }))

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "y8kxln8n",
    "name": "height",
    "type": "number",
    "required": true,
    "presentable": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null,
      "noDecimal": true
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("3zwyqsaoko4dxc5")

  // remove
  collection.schema.removeField("jycjljwj")

  // remove
  collection.schema.removeField("y8kxln8n")

  return dao.saveCollection(collection)
})
