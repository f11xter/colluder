/// <reference path="../pb_data/types.d.ts" />
migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("ue9yrnuo64nyage")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "qgipcbok",
    "name": "name",
    "type": "text",
    "required": true,
    "presentable": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null,
      "pattern": ""
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("ue9yrnuo64nyage")

  // remove
  collection.schema.removeField("qgipcbok")

  return dao.saveCollection(collection)
})
