/// <reference path="../pb_data/types.d.ts" />
migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("37zuz31qh9jv5cw")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "svcw7hnn",
    "name": "isClueStart",
    "type": "bool",
    "required": false,
    "presentable": false,
    "unique": false,
    "options": {}
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("37zuz31qh9jv5cw")

  // remove
  collection.schema.removeField("svcw7hnn")

  return dao.saveCollection(collection)
})
