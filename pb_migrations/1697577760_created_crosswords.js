/// <reference path="../pb_data/types.d.ts" />
migrate((db) => {
  const collection = new Collection({
    "id": "3zwyqsaoko4dxc5",
    "created": "2023-10-17 21:22:40.326Z",
    "updated": "2023-10-17 21:22:40.326Z",
    "name": "crosswords",
    "type": "base",
    "system": false,
    "schema": [
      {
        "system": false,
        "id": "cjbafnnf",
        "name": "title",
        "type": "text",
        "required": false,
        "presentable": false,
        "unique": false,
        "options": {
          "min": null,
          "max": 100,
          "pattern": ""
        }
      },
      {
        "system": false,
        "id": "qnp7fo5i",
        "name": "clues",
        "type": "json",
        "required": true,
        "presentable": false,
        "unique": false,
        "options": {}
      }
    ],
    "indexes": [],
    "listRule": "",
    "viewRule": "",
    "createRule": "",
    "updateRule": "",
    "deleteRule": "",
    "options": {}
  });

  return Dao(db).saveCollection(collection);
}, (db) => {
  const dao = new Dao(db);
  const collection = dao.findCollectionByNameOrId("3zwyqsaoko4dxc5");

  return dao.deleteCollection(collection);
})
