/// <reference path="../pb_data/types.d.ts" />
migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("3zwyqsaoko4dxc5")

  // remove
  collection.schema.removeField("qnp7fo5i")

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("3zwyqsaoko4dxc5")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "qnp7fo5i",
    "name": "clues",
    "type": "json",
    "required": true,
    "presentable": false,
    "unique": false,
    "options": {}
  }))

  return dao.saveCollection(collection)
})
