/// <reference path="../pb_data/types.d.ts" />
migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("37zuz31qh9jv5cw")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "lqobemh4",
    "name": "colour",
    "type": "text",
    "required": false,
    "presentable": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null,
      "pattern": "[0-9a-fA-F]{6}"
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("37zuz31qh9jv5cw")

  // remove
  collection.schema.removeField("lqobemh4")

  return dao.saveCollection(collection)
})
