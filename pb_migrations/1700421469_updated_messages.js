/// <reference path="../pb_data/types.d.ts" />
migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("ue9yrnuo64nyage")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "uvrwxqtg",
    "name": "clue",
    "type": "relation",
    "required": false,
    "presentable": false,
    "unique": false,
    "options": {
      "collectionId": "xaxqr909isea1um",
      "cascadeDelete": true,
      "minSelect": null,
      "maxSelect": 1,
      "displayFields": null
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("ue9yrnuo64nyage")

  // remove
  collection.schema.removeField("uvrwxqtg")

  return dao.saveCollection(collection)
})
